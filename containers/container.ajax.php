<?php
namespace Carbon_Fields\Container;

use Carbon_Fields\Datastore\Datastore;
use Carbon_Fields\Helper\Helper;

/**
 *
 */
class Ajax_Container extends Container {

    /**
     * Array of registered page slugs to verify uniqueness with.
     *
     * @var array
     */
    protected static $registered_pages = array();

    /**
     * {@inheritDoc}
     */
    public function __construct( $id, $title, $type, $condition_collection, $condition_translator ) {

        parent::__construct( $id, $title, $type, $condition_collection, $condition_translator );

        if ( ! $this->get_datastore() ) {
            $this->set_datastore( Datastore::make( 'post_meta' ), $this->has_default_datastore() );
        }

        $this->init();
    }


    /**
     *
     */
    public function init() {

        $input = stripslashes_deep( $_GET );
        //$request_post_id = isset( $input['post'] ) ? intval( $input['post'] ) : 0;


        add_action( 'admin_init', array( $this, '_attach' ) );

    }


    /**
     * Get environment array for page request (in admin).
     *
     * @return array
     */
    protected function get_environment_for_request() {

        if ( !defined( 'DOING_AJAX' ) || !DOING_AJAX ) {
            return array();
        }

        $environment = array();
        $input = stripslashes_deep( $_REQUEST );

        $environment['action'] = isset( $input['action'] ) ? $input['action'] : false;
        $environment['post_id'] = isset( $input['post_id'] ) ? $input['post_id'] : '';
        $environment['post_type'] = get_post_type( $input['post_id'] );

        return $environment;
    }

    /**
     * Perform checks whether the container should be attached during the current request.
     *
     * @return bool True if the container is allowed to be attached.
     */
    public function is_valid_attach_for_request() {
        $input = Helper::input();

        $ajax = defined( 'DOING_AJAX' ) ? DOING_AJAX : false;

        if (!$ajax) {
            return false;
        }

        return $this->static_conditions_pass();
    }

    /**
     * Get environment array for object id.
     *
     * @return array
     */
    protected function get_environment_for_object( $object_id ) {
        echo 90;exit;
        return array();
    }

    /**
     * Check container attachment rules against object id.
     *
     * @param int $object_id
     * @return bool
     */
    public function is_valid_attach_for_object( $object_id = null ) {
        echo 101;exit;
        return true;
    }

    /**
     * Add theme options container pages.
     * Hook the container saving action.
     */
    public function attach() {
        $this->render();
    }

    /**
     * Whether this container is currently viewed.
     *
     * @return boolean
     */
    public function should_activate() {
        return true;
    }

    /**
     * Output the container markup.
     */
    public function render() {
        global $MyPlugin;
        include $MyPlugin->path . 'src/php/definitions/carbon-fields/templates/template.ajax.php';
        exit;
    }

    /**
     * Register the page while making sure it is unique.
     *
     * @return boolean
     */
    protected function register_page() {

    }

    public function is_valid_save() {}
    public function show_on_page() {echo 139;exit;}


}
