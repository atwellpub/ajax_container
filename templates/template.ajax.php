<?php

$fields = $this->get_fields();


foreach ($fields as $field) {
    switch($field->type) {
        case 'text':
            echo '<div class="cf-field cf-text">';
            echo '    <div class="cf-field__head">';
            echo '        <label class="cf-field__label">';
            echo              $field->get_label();
            echo '        </label>';
            echo '    </div>';
            echo '    <div class="cf-field__body">';
            echo '        <input name="" class="cf-text__input" value="">';
            echo '    </div>';
            echo '</div>';
            break;
        case 'textarea':
            echo '<div class="cf-field cf-textarea">';
            echo '    <div class="cf-field__head">';
            echo '        <label class="cf-field__label">';
            echo              $field->get_label();
            echo '        </label>';
            echo '    </div>';
            echo '    <div class="cf-field__body">';
            echo '        <textarea name="" class="cf-textarea__input"></textarea>';
            echo '    </div>';
            echo '</div>';
            break;
        case 'select':
            echo '<div class="cf-field cf-select">';
            echo '    <div class="cf-field__head">';
            echo '        <label class="cf-field__label">';
            echo              $field->get_label();
            echo '        </label>';
            echo '    </div>';
            echo '    <div class="cf-field__body">';
            echo '        <select name="" class="cf-select__input">';
            $options = $field->get_options();
            if ($options = $field->get_options()) {
                foreach($options as $value => $label) {
                    echo '<option value="'.$value.'">'.$label.'</option>';
                }
            }
            echo '        </select>';
            echo '    </div>';
            echo '</div>';
            break;
    }

    /* This action hook allows for custom field types */
    do_action('myplugin/definitions/actions/render_field' , $field->type);
}