<?php
namespace MyPlugin\Definitions\CarbonFields;
use Carbon_Fields\Carbon_Fields;
use Carbon_Fields\Container as Container;
use Carbon_Fields\Condition as Condition;

class Load_Extensions {

    /**
     * https://carbonfields.net/docs/advanced-topics-hooks/
     */
    public function __construct(){

        self::register_ajax_container();


    }

    /**
     *
     */
    public static function register_ajax_container() {
        /* load new container after Carbon Fields is loaded */
        add_action( 'after_setup_theme', array( __CLASS__ , 'load_carbon_field_action_container' ) , 200 );

        /* conditions for ajax container */
        add_filter( 'carbon_fields_ajax_container_static_condition_types' , array(__CLASS__ , 'check_ajax_condition' ) );

    }

    /**
     * @param $condition_types
     * @return array
     */
    public static function check_ajax_condition($condition_types ) {
        return array_merge(
            $condition_types,
            array( 'action' )
        );
    }

    /**
     *
     */
    public static function load_carbon_field_action_container() {

        if ( !is_admin() ) {
            return;
        }

        global $MyPlugin;

        require_once( $MyPlugin->path . 'src/php/definitions/carbon-fields/containers/container.ajax.php');
        require_once( $MyPlugin->path . 'src/php/definitions/carbon-fields/conditions/condition.action.php');

        Carbon_Fields::extend( Condition\Action_Condition::class, function( $container ) {
            $condition = new Condition\Action_Condition();
            $condition->set_comparers( Carbon_Fields::resolve( 'generic', 'container_condition_comparer_collections' ) );
            return $condition;
        });
    }

}

new Load_Extensions();
