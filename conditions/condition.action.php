<?php
/**
 * Carbon fields theme options container modified to support custom files for rendering pages.
 *
 * @package carbon-fields-custom-options-container
 */

namespace Carbon_Fields\Condition;
use Carbon_Fields\Container\Condition\Condition;

class Action_Condition extends Condition {

    /**
     * Check if the condition is fulfilled
     *
     * @param  array $environment
     * @return bool
     */
    public function is_fulfilled( $environment ) {

        return $this->compare(
            $environment['action'],
            $this->get_comparison_operator(),
            $this->get_value()
        );

    }
}